<?php

/*	INSERT STATEMENTS
	// usage directions
	// 1. instantiate database object
	// 2. call the database object's insert() function
	// 3. use an array to pass fields and their values to database
	
	$database = new database();
		or
	$database = new database("my_table"); // using optional parameter to specify table name
	
	// w/ tablename 
	include_once 'bin/database.php';
	$database = new database("my_table");
	$database->insert(array("name"=>"steve", "address_1"=>"666 N horton dr.", "city"=>"huntingburg"));
	echo $database->insert_id;

	// w/o table name
	include_once 'bin/database.php';
	$database = new database();
	$database->insert("my_table", array("name"=>"william", "address_1"=>"666 N devil dr.", "city"=>"huntingburg"));
	echo $database->insert_id;

	// w/ predefined array for fields-values argument
	include_once 'bin/database.php';
	$database = new database();
	$my_array = array("name"=>"william", "address_1"=>"667 N devil dr.", "city"=>"huntingburg");
	$database->insert("my_table", $my_array);
	echo $database->insert_id;	
	
*/	
	// insert id can be obtained by using the database object's insert_id property after insert has been performed
	// example: 
	// echo $database->insert_id;
	// or by just assigning the result of the operation to a variable
	// example: 
	// $insert = $database->insert_id;
	// echo $insert;

/*	SELECT STATEMENTS

	// fetchAll()
	include_once 'bin/database.php';
	$database = new database("my_table");
	$my_array = array("city"=>"huntingburg");
	$result = $database->fetchAll(array("name", "address_1"), $my_array);
	var_dump($result);
	
	// fetch()
	include_once 'bin/database.php';
	$database = new database("my_table");
	$my_array = array("city"=>"huntingburg");
	$result = $database->fetch(array("name", "address_1"), $my_array);
	var_dump($result);	
		
	// count()
	include_once 'bin/database.php';
	$database = new database("users");
	$my_array = array("age"=>"35");
	$result = $database->count($my_array);
	echo $result;	

*/
//	UPDATE STATEMENTS
/*
	include_once 'bin/database.php';
	$database = new database("users");
	$set = array("age"=>"69");
	$where = array("username"=>"grandma");
	$result = $database->update($set, $where);
	echo $result;
	
*/
// 	DELETE STATEMENT
/*
	include_once 'bin/database.php';
	$database = new database();
	$where = array("username"=>"grandma");
	$result = $database->delete("users", $where);
	echo $result;
*/	


?>
