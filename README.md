# README #

The file database.php contains all the code that the project contains currently and is the only file needed to use the scripts.  The first class contained within the file is the database_connection class and it's only role is to produce a PDO object to share or be used within derivative classes or classes that use the database_connection class.  The other classes within the file are the classes that actually address database operations and their usages can be seen in the examples.php file.

### What is this repository for? ###

* Shortcut PDO operations
* PHP Shortcuts in general

### How do I get set up? ###

* Drop the database.php file into a working directory
* Check out the examples.php to understand implementation
* Use it!
* Hack on it and share your improvements, if you want..

### Contribution guidelines ###

* K.I.S.S.
* Provide clear usage and documentation for your changes/additions

### Who do I talk to? ###

* Rob_L (me)
* Join the hipchat channel linc-friends