<?php
class Database_Connection {
	public $dbh;
	private $error;
	// where i'm adding the static variables for this site's database
	private $host = "localhost";	private $dbname = "sandbox";	private $user = "root";	private $pass = "1.Password";
	public function __construct(/* arg_0=host	arg_1=dbname	arg_2=user	arg_3=pass */){
		/* Set DSN */
		$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
		/* Set options */
		$options = array(PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION	);
		/* Create new PDO instanace */	
		try{	$this->dbh = new PDO($dsn, $this->user, $this->pass, $options);	}
		/* Catch errors */				
		catch(PDOException $e){	$this->error = $e->getMessage();				}
	}
}
class insert{

	private $dbh;				private $table;
	private $flds=false;		private $vars=false;
	private $vals = array();	public $row_id;
	
	public function __construct($table, $fields_values){	$this->table = $table;
	
		foreach($fields_values as $key=>$value){
			$this->query_fields($key);
			$this->query_vars($key);
			$this->vals[]=$value;
		}
		
		$db = new Database_Connection();
		$this->dbh = $db->dbh;
		$this->row_id = $this->Insert($fields_values);
	}
	
	private function query_fields($field){
		if($this->flds==false){	$this->flds = "`".$field."`";
		}else{					$this->flds = $this->flds.", `".$field."`";		}
	}
	
	private function query_vars($var){
		if($this->vars==false){	$this->vars = ":".$var;
		}else{					$this->vars = $this->vars.", :".$var;			}
	}	

	private function Insert($params){	$c = 0;
		$query = "INSERT INTO `".$this->table."` (".$this->flds.") VALUES (".$this->vars.")";
		$stmt = $this->dbh->prepare($query);	
		foreach($params as $key=>$value){	$stmt->bindParam(':'.$key, $this->vals[$c]);	$c++;	}
		if($stmt->execute()){	return $this->dbh->lastInsertId();
		}else{					return false;		}	
	}

}
class fetchAll{

	protected $dbh;					protected $table;				protected $query;	
	protected $fields = array();	protected $vars = array();		protected $select_fields = false;
	
	public function __construct($table, $select_fields, $fields_values){	$this->table = $table;
	
		$this->select_fields($select_fields);
	
		foreach($fields_values as $key=>$value){	$this->fields[] = $key;	$this->vars[] = $value;	}

		$this->query = $this->query_string();
		
	}
	
	protected function select_fields($f){
		if(is_array($f)){	$this->fields = array();
			foreach($f as $value){	$this->select_fields[]=$value;	}
		}else{	
			if($f=="all"||$f=="All"||$f=="ALL"){				$this->select_fields="*";	
			}else if($f=="count"||$f=="Count"||$f=="COUNT"){	$this->select_fields="count(*)";	}
		}	
	}
	
	public function query_string(){
		if(is_array($this->select_fields)){			$first = false;
			foreach($this->select_fields as $field){ if($first==false){ $first = $field; }else{ $first = $first.", ".$field; }	}
		}else{	$first = $this->select_fields;	}	$p = false;
		foreach($this->fields as $field){ if($p==false){ $p = $field." = :".$field; }else{ $p = $p." AND ".$field." = :".$field; } }	
		return "SELECT ".$first." FROM ".$this->table." WHERE ".$p;
	}
	
	public function Select(){		$c=0;
		$stmt = $this->dbh->prepare($this->query);	
		foreach($this->fields as $value){	$stmt->bindParam(':'.$value, $this->vars[$c]);	$c++;	}
		if($stmt->execute()){	return $stmt->fetchAll();	}else{	return false;	}
	}
	
	public function exe(){
		$db = new Database_Connection();
		$this->dbh = $db->dbh;
		$this->result = $this->Select();
	}

}
class fetch extends fetchAll{

	public function __construct($table, $select_fields, $fields_values){	$this->table = $table;
	
		$this->select_fields($select_fields);
	
		foreach($fields_values as $key=>$value){	$this->fields[] = $key;	$this->vars[] = $value;	}

		$this->query = $this->query_string();
		
	}
	
	public function Select(){		$c=0;
		$stmt = $this->dbh->prepare($this->query);	
		foreach($this->fields as $value){	$stmt->bindParam(':'.$value, $this->vars[$c]);	$c++;	}
		if($stmt->execute()){	return $stmt->fetch();
		}else{					return false;		}
	}
	
	public function exe(){
		$db = new Database_Connection();
		$this->dbh = $db->dbh;
		$this->result = $this->Select();
	}

}
class count extends fetchAll{

	public function __construct($table, $fields_values){	$this->table = $table;
	
		foreach($fields_values as $key=>$value){	$this->fields[] = $key;	$this->vars[] = $value;	}

		$this->query = $this->query_string();
		
	}
	
	public function query_string(){		$str = "SELECT count(*)";	$p = false;
		foreach($this->fields as $field){
			if($p==false){	$p = $field." = :".$field;
			}else{			$p = $p." AND ".$field." = :".$field;	}
		}	return $str." FROM ".$this->table." WHERE ".$p;
	}	
	
	public function Select(){		$c=0;
		$stmt = $this->dbh->prepare($this->query);	
		foreach($this->fields as $value){	$stmt->bindParam(':'.$value, $this->vars[$c]);	$c++;	}
		if($stmt->execute()){	return $stmt->fetchColumn();
		}else{					return false;		}
	}
	
	public function exe(){
		$db = new Database_Connection();
		$this->dbh = $db->dbh;
		$this->result = $this->Select();
	}

}

class update{

	private $dbh;					private $table;					private $query;
	private $set_cols = array();	private $set_vals = array();
	private $w_cols = array();		private $w_vals = array();
	
	public function __construct($table, $set_fields, $where_fields){	
		$this->table = $table;
		$this->set_fields_worker($set_fields);	
		$this->where_worker($where_fields);	
		$this->query = $this->query_string();
	}
	
	private function where_worker($fields){			foreach($fields as $key => $value){	$this->w_cols[] = $key;		$this->w_vals[] = $value;	}	}
	
	private function set_fields_worker($fields){	foreach($fields as $key => $value){	$this->set_cols[] = $key;	$this->set_vals[] = $value;	}	}
	
	private function query_string(){	$str = "UPDATE ".$this->table." SET ";	$a = false;	$w = false;
		foreach($this->set_cols as $val){	if($a==false){	$a = $val." = :".$val;		}else{	$a = $a.", ".$val." = :".$val;		}	}
		foreach($this->w_cols as $cols){	if($w==false){	$w = $cols." = :".$cols;	}else{	$w = $w." AND ".$cols." = :".$cals;	}	}
		return $str.$a." WHERE ".$w;
	}
	
	private function Update(){		$c=0;$cc=0;		$stmt = $this->dbh->prepare($this->query);	
		foreach($this->set_cols as $value){			$stmt->bindParam(':'.$value, $this->set_vals[$c]);	$c++;	}
		foreach($this->w_cols as $value){			$stmt->bindParam(':'.$value, $this->w_vals[$cc]);	$cc++;	}
		if($stmt->execute()){	return true;	}else{	return false;	}
	}
	
	public function exe(){	
		$db = new Database_Connection();	$this->dbh = $db->dbh;
		return $this->Update();
	}	

}
class delete{

	private $dbh;					private $table;					private $query;
	private $w_cols = array();		private $w_vals = array();
	
	public function __construct($table, $where_fields){	
		$this->table = $table;
		$this->where_worker($where_fields);	
		$this->query = $this->query_string();
	}	
	
	private function where_worker($fields){			foreach($fields as $key => $value){	$this->w_cols[] = $key;		$this->w_vals[] = $value;	}	}
	
	private function query_string(){		$w = false;
		foreach($this->w_cols as $cols){	if($w==false){	$w = $cols." = :".$cols;	}else{	$w = $w." AND ".$cols." = :".$cals;	}	}
		return "DELETE FROM ".$this->table." WHERE ".$w;
	}	
	
	private function Delete(){				$c=0;
		$stmt = $this->dbh->prepare($this->query);
		foreach($this->w_cols as $value){			$stmt->bindParam(':'.$value, $this->w_vals[$c]);	$c++;	}
		if($stmt->execute()){	return true;	}else{	return false;	}
	}
	
	public function exe(){	
		$db = new Database_Connection();	$this->dbh = $db->dbh;
		return $this->Delete();
	}		

}
class database{

	public $table;
	
	public function __construct(){	if(func_num_args()>0){	$this->table = func_get_arg(0);	}	}	

	public function insert(){
		if(func_num_args()>1){	$insert = new insert(func_get_arg(0), func_get_arg(1));
		}else{					$insert = new insert($this->table, func_get_arg(0));		}
		return $insert->row_id;
	}
	
	public function fetchAll(){
		if(func_num_args()>2){	$select = new fetchAll(func_get_arg(0), func_get_arg(1), func_get_arg(2));
		}else{					$select = new fetchAll($this->table, func_get_arg(0), func_get_arg(1));		
		}
		$select->exe();	return $select->result;	
	}
	
	public function fetch(){
		if(func_num_args()>2){	$select = new fetch(func_get_arg(0), func_get_arg(1), func_get_arg(2));
		}else{					$select = new fetch($this->table, func_get_arg(0), func_get_arg(1));		
		}
		$select->exe();	return $select->result;
	}
	
	public function count(){
		if(func_num_args()>1){	$select = new count(func_get_arg(0), func_get_arg(1));
		}else{					$select = new count($this->table, func_get_arg(0));		
		}
		$select->exe();	return $select->result;
	}
	
	public function update(){
		if(func_num_args()>2){	$update = new update(func_get_arg(0), func_get_arg(1), func_get_arg(2));
		}else{					$update = new update($this->table, func_get_arg(0), func_get_arg(1));		
		}
		return $update->exe();
	}

	public function delete(){
		if(func_num_args()>1){	$delete = new delete(func_get_arg(0), func_get_arg(1));
		}else{					$delete = new delete($this->table, func_get_arg(0));		
		}	return $delete->exe();
	}

}
?> 


